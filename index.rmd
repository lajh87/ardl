[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/ardl)](https://cran.r-project.org/package=ardl)

# Installation

To install the package use the following command.

```{r,eval=F}
devtools::install_git("git@gitlab.com:lajh87/ardl.git")
```
